class User < ApplicationRecord
  has_secure_password

  validates :email, presence: true, uniqueness: true

  has_many :sessions, dependent: :destroy, inverse_of: :user
end
