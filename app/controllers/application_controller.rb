class ApplicationController < ActionController::Base

  def current_session
    unless defined? @current_session
      @current_session = Session.find_by(token: cookies['_session_id'])
    end

    @current_session
  end
  helper_method :current_session

  def current_user
    current_session&.user
  end
  helper_method :current_user
end
